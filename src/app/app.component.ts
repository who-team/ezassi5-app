import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  selector: string = '.main-panel';

  elements = Array(10)

  onScroll() {
    this.elements = Array(this.elements.length + 10)
  }


}
