import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

import { AngularFontAwesomeModule } from 'angular-font-awesome';
import {StarRatingModule} from 'angular-star-rating';
import { ExtraInfoComponent } from './extra-info/extra-info.component';
import { BasicInfoComponent } from './basic-info/basic-info.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { FlexLayoutModule } from '@angular/flex-layout';



@NgModule({
  declarations: [
    AppComponent,
    ExtraInfoComponent,
    BasicInfoComponent
  ],
  imports: [
    BrowserModule,

    AngularFontAwesomeModule,
    FormsModule,
    StarRatingModule.forRoot(),
    InfiniteScrollModule,
    FlexLayoutModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

platformBrowserDynamic().bootstrapModule(AppModule);
